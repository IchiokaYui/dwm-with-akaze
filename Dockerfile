FROM jjanzic/docker-python3-opencv
RUN set -x && \
    wget http://johnvansickle.com/ffmpeg/releases/ffmpeg-release-64bit-static.tar.xz && \
    tar xvf ffmpeg-release-64bit-static.tar.xz && \
    cp ./ffmpeg-3.4.1-64bit-static/ffmpeg /usr/local/bin && \
    cp ./ffmpeg-3.4.1-64bit-static/ffprobe /usr/local/bin && \
    pip install scipy && \
    pip install pillow
