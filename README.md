# 画像特徴量に基づく同期回復を用いた画像,動画電子透かし
質問など御座いましたら、できる範囲でお答え致します。

# フォルダ構成
|ファイル,フォルダ名|概要|
|-|-|
|main|メインのプログラムが入っているフォルダ|
|.gitignore|Gitの管理に含めないファイルを指定するためのファイル|
|Dockerfile|Dockerの設定ファイル|
|LICENSE.md|ライセンスファイル|
|README.md|このファイル(説明ファイル)|

各プログラムのフォルダには同様に説明ファイル(README.md)が入っている。

# 使い方
Dockerをインストールして,以下のコマンドを実行。
実行環境が構築されたコンテナ(仮想環境)を使うことができる。
path-toは各実行環境に応じて書き換える。
run後に仮想環境に入ることができる。

build
```
cd dwm-with-akaze
sudo docker build -t y-ichioka/dwm-with-akaze .
```

run(シングルコアの場合)
```
sudo docker run -i -t --rm -v /path-to/dwm-with-akaze/main:/main y-ichioka/dwm-with-akaze /bin/bash
```

run(マルチコアの場合)
- --cpuset-cpusで任意の値を設定できる
- Vagrantを使ってる場合はVagrantfileの設定に注意

```
sudo docker run -i -t --rm --cpuset-cpus 0-3 -v /path-to/dwm-with-akaze/main:/main y-ichioka/dwm-with-akaze /bin/bash
```

# Licence
The MIT License (MIT)

