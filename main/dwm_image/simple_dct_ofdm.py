# coding: UTF-8

# 量子化DCT-OFDM方式を用いて埋め込み抽出を行うプログラム
# 画像は正方形が前提
# 画像全体をDCTして行う(つまりブロック化などして冗長化はしない)
# 以下のようにコマンドライン引数を指定して扱う
# python simple_dct_ofdm.py EMBED input (out)
# python simple_dct_ofdm.py EXTRACT input

import image_processing as ip
import matrix_sequence_linker as msl
import sequence_processing as sp
import dwm

SEED = 16606
MIN = 0.25
MAX = 0.5
USE_LEN = 256
EMBED_STR = 50
SECRET_INFO = [-1]*50 + [+1]*50
PN_SERIES = sp.pn_series(USE_LEN, seed=SEED)

def embed(input_name, output_name):
    # 周波数成分の取得
    y, cr, cb = ip.get_ycbcr_array(input_name)
    y = ip.dct2d(y)

    # 周波数成分から系列を作成
    seq = msl.get_mid_freq_seq(y, _min=MIN, _max=MAX)
    seq = sp.resettable_shuffle(seq, SEED)
    use_array, not_use_array = sp.split_into_two(seq, USE_LEN)

    # 量子化DCT-OFDM方式を用いて埋め込み
    use_array = ip.dct1d(use_array * PN_SERIES)
    sp.embed_by_qim(use_array, SECRET_INFO, EMBED_STR)
    use_array = ip.idct1d(use_array) * PN_SERIES

    # 系列を周波数成分に戻す
    seq = sp.joint_two(use_array, not_use_array)
    seq = sp.reset_shuffle(seq, SEED)
    msl.set_mid_freq_seq(y, seq, _min=MIN, _max=MAX)
    y = ip.idct2d(y)

    # 画素値を0から255内に設定して保存
    msl.set_in_range(y, 0, 255)
    ip.save_ycbcr_as_img(y, cr, cb, output_name)

def extract(input_name):
    # 周波数成分の取得
    y, cr, cb = ip.get_ycbcr_array(input_name)
    y = ip.dct2d(y)

    # 周波数成分から系列を作成
    seq = msl.get_mid_freq_seq(y, _min=MIN, _max=MAX)
    seq = sp.resettable_shuffle(seq, SEED)
    use_array, not_use_array = sp.split_into_two(seq, USE_LEN)

    # 量子化DCT-OFDM方式を用いて埋め込み
    use_array = ip.dct1d(use_array * PN_SERIES)

    # 以上までembed()と同じ
    # 以下抽出処理
    info = sp.extract_by_qim(use_array, len(SECRET_INFO), EMBED_STR)
    print("BER: %lf" % (sp.hamming_dist(SECRET_INFO, info) / float(len(SECRET_INFO))))

if __name__ == "__main__":
    dwm.branch(embed, extract)

