# coding: UTF-8

# 行列と数列に関する処理が記述されたmodule
# 行列から数列を取り出したり、入れたり

import numpy as np
import math

# ref_arrayは2次元の正方行列
# 2次元行列の左上を低周波、右下を高周波と定義したとき、この関数は中間の周波数成分を系列(普通のリスト)として出力する
# _minと_maxは_min<_maxでなくてはならない
# _minと_maxは0以上1以下
# _minと_maxの差が中間の周波数成分の量となる
# もっと高速化できる
# 未チェック
# 非破壊的メソッド
def get_mid_freq_seq(ref_array, _min, _max):
    ret_seq = []
    width = len(ref_array[0])
    height = len(ref_array)
    tmp_min = _min*(width+height)
    tmp_max = _max*(width+height)
    for y in range(height):
        for x in range(width):
            if tmp_min <= x+y <= tmp_max:
                ret_seq.append(ref_array[y][x])
    return ret_seq

# get_mid_freq_seqとほとんど一緒
# _minと_maxを比率でなく、ピクセル単位で設定
def get_mid_freq_seq2(ref_array, _min, _max):
    ret_seq = []
    width = len(ref_array[0])
    height = len(ref_array)
    tmp_min = _min
    tmp_max = _max
    for y in range(height):
        for x in range(width):
            if tmp_min <= x+y <= tmp_max:
                ret_seq.append(ref_array[y][x])
    return ret_seq

# ref_arrayは2次元の正方行列
# もっと高速化できる
# get_mid_freq_seqの逆操作を行う
# 指定したseqを順番に中間の周波数成分にセットしていく
# 未チェック
# 破壊的メソッド
def set_mid_freq_seq(ref_array, seq, _min, _max):
    width = len(ref_array[0])
    height = len(ref_array)
    tmp_min = _min*(width+height)
    tmp_max = _max*(width+height)
    count = 0
    for y in range(height):
        for x in range(width):
            if tmp_min <= x+y <= tmp_max:
                ref_array[y][x] = seq[count]
                count += 1

# set_mid_freq_seqとほとんど一緒
# _minと_maxを比率でなく、ピクセル単位で設定
def set_mid_freq_seq2(ref_array, seq, _min, _max):
    width = len(ref_array[0])
    height = len(ref_array)
    tmp_min = _min
    tmp_max = _max
    count = 0
    for y in range(height):
        for x in range(width):
            if tmp_min <= x+y <= tmp_max:
                ref_array[y][x] = seq[count]
                count += 1

# ref_arrayは2次元の正方行列
# ref_arrayの要素が_minより小さな値だったら_minに、
# _maxより大きな値だったら_maxに値を変える。それ以外の場合はなにもしない
# もっと高速化できる(?)
# 未チェック
# 破壊的メソッド
def set_in_range(ref_array, _min, _max):
    for y in range(len(ref_array)):
        for x in range(len(ref_array[0])):
            if ref_array[y][x] < _min:
                ref_array[y][x] = _min
            if ref_array[y][x] > _max:
                ref_array[y][x] = _max

# それぞれの要素がその要素の番地のタプル(y,x)を持っている2次元行列を返す
# 2次元行列はsize*size
# もっと高速化できる(?)
# 非破壊的メソッド
def address_2dmatrix(size):
    ret = []
    for y in range(size):
        ret.append([None]*size)
        for x in range(size):
            ret[y][x] = (y,x)
    return ret

# それぞれの要素が番地のタプル(y,x)を持っている1次元の配列(address_array)から
# ret_array(2次元)を参照し,その番地の値を持つ1次元配列を返す
# 返す配列は1次元で長さはaddress_arrayと同じになる
# もっと高速化できる(?)
# 非破壊的メソッド
def get_seq_by_address(ref_array, address_array):
    ret = [None] * len(address_array)
    for i in range(len(ret)):
        y = address_array[i][0]
        x = address_array[i][1]
        ret[i] = ref_array[y][x]
    return ret

# ref_arrayは2次元の正方行列
# get_seq_by_addressの逆操作を行う
# もっと高速化できる(?)
# 破壊的メソッド(ref_arrayを破壊する)
def set_seq_by_address(ref_array, address_array, seq):
    if len(seq) != len(address_array):
        raise Exception("Array with correct length is not set.")

    for pointer in range(len(address_array)):
        y = address_array[pointer][0]
        x = address_array[pointer][1]
        ref_array[y][x] = seq[pointer]

# sizeの二次元配列を返す
# 指定したstart_pt=(x,y)を基準としてtemplate_array(2次元配列)を敷き詰めた二次元配列を返す
# 非破壊的メソッド
def array_paved_pattern(size, start_pt, template_array):
    width, height = size
    start_x, start_y = start_pt
    ret = [[None for i in range(width)] for j in range(height)]

    tmp_h = len(template_array)
    tmp_w = len(template_array[0])

    for y in range(height):
        for x in range(width):
            ret[y][x] = template_array[(y-start_y)%tmp_h][(x-start_x)%tmp_w]

    return ret

# ref_arrayは参照される二次元配列
# 二次元配列から多数の正方形の二次元行列を切り抜き、それを二次元行列の内部に格納してそれを返す
# start_pt=(x,y)で指定される座標を起点としてblock_size*block_sizeの二次元行列を切り抜く
# 返り値は切り抜いた正方形行列の2次元行列Aと、起点となる正方形行列の場所(行列Aの番地)と,取り出しはじめの一番左上の座標
# 返すそれぞれの二次元行列はnumpyのarray
# 非破壊的メソッド
def crop_blocks(ref_array, block_size, start_pt):
    tmp_array = np.array(ref_array)
    height, width = tmp_array.shape
    start_x, start_y = start_pt
    start_x = math.floor(start_x)
    start_y = math.floor(start_y)

    upper_left_w = math.floor(start_x/block_size)
    upper_left_h = math.floor(start_y/block_size)
    w_num = upper_left_w + math.floor((width-start_x)/block_size)
    h_num = upper_left_h + math.floor((height-start_y)/block_size)
    tmp_x = start_x - upper_left_w*block_size
    tmp_y = start_y - upper_left_h*block_size

    # print(upper_left_w, upper_left_h)
    # print(w_num, h_num)
    # print(tmp_x, tmp_y)

    # 2次元配列の静的確保
    blocks = [[None for i in range(w_num)] for j in range(h_num)]

    # blocksに代入していく
    for y in range(h_num):
        for x in range(w_num):
            _start_y = tmp_y + y*block_size
            _end_y = _start_y + block_size
            _start_x = tmp_x + x*block_size
            _end_x = _start_x + block_size
            blocks[y][x] = tmp_array[_start_y:_end_y, _start_x:_end_x]

    return [blocks, (upper_left_w, upper_left_h), (tmp_x, tmp_y)]

# crop_blocks()の切り抜きが一部のバージョン
# 恐らく使わないので,メモとして残す
# UPPER_RIGHT = 0
# UPPER_LEFT = 1
# LOWER_LEFT = 2
# LOWER_RIGHT = 3
# def part_of_crop_blocks(ref_array, block_size, start_pt, position):
#     tmp_array = np.array(ref_array)
#     height, width = tmp_array.shape
#     start_x, start_y = start_pt

#     w_num = None
#     h_num = None
#     tmp_x = None
#     tmp_y = None

#     if position == UPPER_RIGHT:
#         w_num = math.floor((width-start_x)/block_size)
#         h_num = math.floor(start_y/block_size)
#         tmp_x = start_x
#         tmp_y = start_y - h_num*block_size
#     elif position == UPPER_LEFT:
#         w_num = math.floor(start_x/block_size)
#         h_num = math.floor(start_y/block_size)
#         tmp_x = start_x - w_num*block_size
#         tmp_y = start_y - h_num*block_size
#     elif position == LOWER_RIGHT:
#         w_num = math.floor((width-start_x)/block_size)
#         h_num = math.floor((height-start_y)/block_size)
#         tmp_x = start_x
#         tmp_y = start_y
#     elif position == LOWER_LEFT:
#         w_num = math.floor(start_x/block_size)
#         h_num = math.floor((height-start_y)/block_size)
#         tmp_x = start_x - w_num*block_size
#         tmp_y = start_y
#     else:
#         raise Exception("Please set correct arguments.")

#     # 2次元配列の静的確保
#     blocks = [[None for i in range(w_num)] for j in range(h_num)]

#     # blocksに代入していく
#     for y in range(h_num):
#         for x in range(w_num):
#             _start_y = tmp_y + y*block_size
#             _end_y = _start_y + block_size
#             _start_x = tmp_x + x*block_size
#             _end_x = _start_x + block_size
#             blocks[y][x] = tmp_array[_start_y:_end_y, _start_x:_end_x]
#     return blocks

# crop_blocks()の逆操作
# target_arrayに切り抜いたblocksを張り付ける
# 破壊的メソッド
def paste_crop_blocks(target_array, start_pt, blocks):
    w_margin, h_margin = start_pt
    w_margin = math.floor(w_margin)
    h_margin = math.floor(h_margin)

    block_size = len(blocks[0][0])

    for y in range(len(blocks)):
        for x in range(len(blocks[0])):
            start_y = h_margin + y*block_size
            end_y = start_y + block_size
            start_x = w_margin + x*block_size
            end_x = start_x + block_size
            target_array[start_y:end_y, start_x:end_x] = blocks[y][x]
