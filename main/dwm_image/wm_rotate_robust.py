# coding: UTF-8

# 以下のようにコマンドライン引数を指定して扱う
# python qdoa.py EMBED input (out)
# python qdoa.py EXTRACT input

import image_processing as ip
import matrix_sequence_linker as msl
import rotate_robust as rr
import dwm
import akaze
import numpy as np
import quality_metrics as qm
import sys
import time
import cv2
import ss_dwm as ss
import itertools

# 埋め込み基準点の数と推定埋め込み基準点の数
BASIS_POINTS_NUM = 5
BASIS_POINTS_NUM_EX = 10

MIN_BLOCK_SIZE = 300

# ss_dwm用パラメータ
N_OF_SEQ_LEN = 75000 # 25000
RANDOM_SEED = 123456
BIT_NUM = 2**8
A_OF_STR = 0.3
EMBED_DATA = ss.pn_series(16, 111111)
MID_FREQ_MIN = 60
MID_FREQ_MAX = 300


def embed(input_name, output_name, defalut_fps=None):
    print("input_name", input_name)

    # 画像読み込み及び色空間を変換したものの取得
    width, height = ip.get_size(input_name)
    y, cr, cb = ip.get_ycbcr_array(input_name)

    # SS用、埋め込み用データ
    X_OF_ENC = ss.pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED)
    Y_OF_ENC = ss.pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED+1)
    X_INDEX = ss.bin_array_to_num(EMBED_DATA[:8])
    Y_INDEX = ss.bin_array_to_num(EMBED_DATA[8:])
    print("EMBED_DATA:", EMBED_DATA)
    print("X_INDEX:", EMBED_DATA[:8], X_INDEX)
    print("Y_INDEX:", EMBED_DATA[8:], Y_INDEX)

    # # AKAZE特徴量デバッグ用
    # basis_points = akaze.robust_fps(input_name, BASIS_POINTS_NUM)
    # akaze.imwrite_fps(input_name, basis_points)
    # akaze.print_fps(basis_points)
    # basis_points = None

    # タイマはじまり
    time_start = time.time()

    # 画像特徴点(BASIS_POINTS_NUM個)の取得
    basis_points = akaze.robust_fps(input_name, BASIS_POINTS_NUM)
    # akaze.imwrite_fps(input_name, basis_points)
    # akaze.print_fps(basis_points)

    # 回転ロバスト系
    fps = basis_points
    if defalut_fps != None:
        fps = defalut_fps

    # akaze.imwrite_fps(img_path, fps)
    ranks = rr.fps_dist_ranks(fps)

    # 特徴点の組み合わせ
    base_p1 = None
    base_p2 = None
    fps_comb = list(itertools.combinations(fps, 2))
    for idx, fps_set in enumerate(fps_comb):
        tmp_fp1 = fps_set[0].pt
        tmp_fp2 = fps_set[1].pt

        np_pt1 = np.array(tmp_fp1)
        np_pt2 = np.array(tmp_fp2)
        dist = np.linalg.norm(np_pt2 - np_pt1)

        if MIN_BLOCK_SIZE < dist:
            base_p1 = np_pt1
            base_p2 = np_pt2
            break
    else:
        print("有効な特徴点の組み合わせが見つかりませんでした.")
        return False
        # sys.exit()

    # 画像が切れないように枠を設置した画像を作成
    frame_size, field_img = rr.get_field_image(y)
    # akaze.print_fp(fps[0])
    # akaze.print_fp(ranks[0][2])

    p1 = base_p1 + np.array([frame_size, frame_size])
    p2 = base_p2 + np.array([frame_size, frame_size])
    int_p1 = tuple(map(int, p1))
    int_p2 = tuple(map(int, p2))
    # cv2.line(field_img, int_p1, int_p2, (255, 255, 0), 2)
    cv2.imwrite("field0.png", field_img)

    # 回転する角度
    angle = rr.vec_angle(p1, p2)
    print("angle:", angle)

    # 角度に合わせて回転(ベクトルが水平になる)
    field_img = rr.rotate_img(field_img, -angle)
    p1_aft = rr.rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p1)
    p2_aft = rr.rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p2)
    print("p1_aft:", p1_aft)
    print("p2_aft:", p2_aft)
    # int_p1_aft = tuple(map(int, p1_aft))
    # int_p2_aft = tuple(map(int, p2_aft))
    # cv2.line(field_img, int_p1_aft, int_p2_aft, (0, 255, 255), 2)
    cv2.imwrite("field1.png", field_img)

    # 埋め込み
    basis_point = p1_aft
    local_block_size = int(round(p2_aft[0] - p1_aft[0]))
    print("local_block_size:", local_block_size) # 正の値になるはず

    # 矩形取り出し、DCT
    start_x = int(round(min(p1_aft[0], p2_aft[0])))
    end_x   = start_x + local_block_size
    start_y = int(round(min(p1_aft[1], p2_aft[1])))
    end_y   = start_y + local_block_size
    crop_block = field_img[start_y:end_y, start_x:end_x]
    crop_block = ip.dct2d(crop_block)

    # SS、乱数列の足しこみ
    mid_freq = msl.get_mid_freq_seq2(crop_block, MID_FREQ_MIN, MID_FREQ_MAX)
    print("len(mid_freq):", len(mid_freq))
    if len(mid_freq) > N_OF_SEQ_LEN: # 警告
        print("N_OF_SEQ_LENがlen(mid_freq)より小さいです.")
    tmp_x = X_OF_ENC[X_INDEX][:len(mid_freq)]
    tmp_y = Y_OF_ENC[Y_INDEX][:len(mid_freq)]
    mid_freq = mid_freq + A_OF_STR * np.absolute(mid_freq) * ((tmp_x + tmp_y)/2)
    msl.set_mid_freq_seq2(crop_block, mid_freq, MID_FREQ_MIN, MID_FREQ_MAX)

    # IDCT、矩形をもどす
    crop_block = ip.idct2d(crop_block)
    field_img[start_y:end_y, start_x:end_x] = crop_block

    # 逆回転
    cv2.imwrite("field2.png", field_img)
    field_img = rr.rotate_img(field_img, angle)
    cv2.imwrite("field3.png", field_img)

    # 切り抜き
    field_img = rr.trim_field_image(field_img, frame_size, [height, width])

    # タイマ終わり
    elapsed_time = time.time() - time_start
    print("TIME\t{0}".format(elapsed_time))

    # 保存
    ip.save_ycbcr_as_img(field_img, cr, cb, output_name)

    # 評価
    print("PSNR\t%f" % qm.psnr_by_file(input_name, output_name))
    # print("SSIM\t%f" % qm.ssim_by_file(input_name, output_name))

    return True

def extract(input_name):
    print("input_name", input_name)

    # 画像読み込み及び色空間を変換したものの取得
    width, height = ip.get_size(input_name)
    y, cr, cb = ip.get_ycbcr_array(input_name)

    # SS用、埋め込み用データ
    X_OF_ENC = ss.pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED)
    Y_OF_ENC = ss.pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED+1)
    X_INDEX = ss.bin_array_to_num(EMBED_DATA[:8])
    Y_INDEX = ss.bin_array_to_num(EMBED_DATA[8:])
    print("EMBED_DATA:", EMBED_DATA)
    print("X_INDEX:", EMBED_DATA[:8], X_INDEX)
    print("Y_INDEX:", EMBED_DATA[8:], Y_INDEX)

    # タイマはじまり
    time_start = time.time()

    # 画像特徴点(BASIS_POINTS_NUM個)の取得
    basis_points = akaze.robust_fps(input_name, BASIS_POINTS_NUM_EX)
    # akaze.imwrite_fps(input_name, basis_points)
    # akaze.print_fps(basis_points)

    # 回転ロバスト系
    # 特徴点
    fps = basis_points
    # akaze.imwrite_fps(img_path, fps)
    ranks = rr.fps_dist_ranks(fps)

    # ループ用
    def try_extract_fps_set(base_p1, base_p2):
        # 画像が切れないように枠を設置した画像を作成
        frame_size, field_img = rr.get_field_image(y)
        p1 = base_p1 + np.array([frame_size, frame_size])
        p2 = base_p2 + np.array([frame_size, frame_size])
        int_p1 = tuple(map(int, p1))
        int_p2 = tuple(map(int, p2))
        # cv2.line(field_img, int_p1, int_p2, (255, 255, 0), 2)
        cv2.imwrite("field0.png", field_img)

        # 回転する角度
        angle = rr.vec_angle(p1, p2)
        print("angle:", angle)

        # 角度に合わせて回転(ベクトルが水平になる)
        field_img = rr.rotate_img(field_img, -angle)
        p1_aft = rr.rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p1)
        p2_aft = rr.rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p2)
        print("p1_aft:", p1_aft)
        print("p2_aft:", p2_aft)
        # int_p1_aft = tuple(map(int, p1_aft))
        # int_p2_aft = tuple(map(int, p2_aft))
        # cv2.line(field_img, int_p1_aft, int_p2_aft, (0, 255, 255), 2)
        cv2.imwrite("field1.png", field_img)

        # 埋め込み
        basis_point = p1_aft
        local_block_size = int(round(p2_aft[0] - p1_aft[0]))
        print("local_block_size:", local_block_size) # 正の値になるはず

        # 矩形取り出し、DCT
        start_x = int(round(min(p1_aft[0], p2_aft[0])))
        end_x   = start_x + local_block_size
        start_y = int(round(min(p1_aft[1], p2_aft[1])))
        end_y   = start_y + local_block_size
        crop_block = field_img[start_y:end_y, start_x:end_x]
        crop_block = ip.dct2d(crop_block)

        # 相関
        mid_freq = msl.get_mid_freq_seq2(crop_block, MID_FREQ_MIN, MID_FREQ_MAX)
        corre_x = []
        corre_y = []
        for idx in range(BIT_NUM):
            tmp_x = X_OF_ENC[idx][:len(mid_freq)]
            tmp_y = Y_OF_ENC[idx][:len(mid_freq)]
            corre_x.append( np.dot(tmp_x, mid_freq) )
            corre_y.append( np.dot(tmp_y, mid_freq) )

        corre_x = np.array(corre_x) / len(mid_freq)
        corre_y = np.array(corre_y) / len(mid_freq)

        # # 閾値を超える番地を探して、抽出
        # threshold = 1/(20*len(mid_freq)) * np.absolute(mid_freq).sum()
        # x_index = np.where(corre_x > threshold)
        # y_index = np.where(corre_y > threshold)

        print(np.argmax(corre_x), np.argmax(corre_y))

        # 最大の相関値とその番地を返す
        return [
            (np.argmax(corre_x), np.max(corre_x)),
            (np.argmax(corre_y), np.max(corre_y))
        ]


    # 特徴点の組み合わせ
    results_x = []
    results_y = []
    fps_comb = list(itertools.combinations(fps, 2))
    for idx, fps_set in enumerate(fps_comb):
        tmp_fp1 = fps_set[0].pt
        tmp_fp2 = fps_set[1].pt

        np_pt1 = np.array(tmp_fp1)
        np_pt2 = np.array(tmp_fp2)
        dist = np.linalg.norm(np_pt2 - np_pt1)

        if MIN_BLOCK_SIZE < dist:
            print("try:", idx+1, np_pt1, np_pt2)
            result_x, result_y = try_extract_fps_set(np_pt1, np_pt2)
            results_x.append(result_x)
            results_y.append(result_y)

    # 最大の相関値を持つインデックスが透かし情報
    results_x = sorted(results_x, key=lambda a: a[1], reverse=True)
    results_y = sorted(results_y, key=lambda a: a[1], reverse=True)
    # print(results_x)
    # print(results_y)
    try:
        print(results_x[0])
        print(results_y[0])
        success = (results_x[0][0] == X_INDEX) and (results_y[0][0] == Y_INDEX)
    except:
        success = False

    # タイマ終わり
    elapsed_time = time.time() - time_start
    print("TIME\t{0}".format(elapsed_time))

    return success

if __name__ == "__main__":
    dwm.branch(embed, extract)
