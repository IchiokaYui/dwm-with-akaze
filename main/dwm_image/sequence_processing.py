# coding: UTF-8

# 数列処理に関する処理が記述されたmodule

import numpy as np
import random
import math

# info1とinfo2とのハミング距離を返す
# https://ja.wikipedia.org/wiki/%E3%83%8F%E3%83%9F%E3%83%B3%E3%82%B0%E8%B7%9D%E9%9B%A2
# 非破壊的メソッド
def hamming_dist(info1, info2):
    dist = 0
    for i in range(len(info1)):
        if info1[i] != info2[i]:
            dist += 1
    return dist

# info1とinfo2とのBERを返す
# 非破壊的メソッド
def ber(info1, info2):
    return hamming_dist(info1, info2) / float(len(info1))

# +1と-1から成るnumpyの配列を2進数として解釈して,10進数のintで返す
# 非破壊的メソッド
def bin_array_to_int(array):
    tmp = np.copy(array)
    binarization(tmp)
    tmp = tmp.tolist()
    return int("".join(map(str, tmp)), 2)

# +1と-1から成るlength長の乱数系列(numpyの1次元行列)を返す
# seedを指定した場合,seedで初期化する
# 非破壊的メソッド
def pn_series(length, seed=None):
    if seed != None:
        random.seed(seed)

    out_series = []
    for i in range(length):
        if random.randint(0,1) == 0:
            out_series.append(-1)
        else:
            out_series.append(+1)
    return np.array(out_series)

# リセットできるシャッフルを行う
# arrayは普通の1次元のリスト(出力も普通の1次元のリスト)
# http://qiita.com/EafT/items/9527cb30409b70106fd4
# 非破壊的メソッド
def resettable_shuffle(array, seed):
    a = np.copy(array)
    np.random.seed(seed)
    np.random.shuffle(a)
    return np.ndarray.tolist(a)

# リセットできるシャッフルを行ったものをリセットする
# arrayは普通の1次元のリスト(出力も普通の1次元のリスト)
# http://qiita.com/EafT/items/9527cb30409b70106fd4
# 非破壊的メソッド
def reset_shuffle(array, seed):
    seq = np.arange(len(array))
    np.random.seed(seed)
    np.random.shuffle(seq)
    tmp = np.c_[seq.T, np.array(array).T]
    tmp = np.ndarray.tolist(tmp)
    tmp = sorted(tmp)
    tmp = np.array(tmp)
    return np.ndarray.tolist(tmp[:,1])

# 整数のみで構成される数列を(正の整数,負の整数)->(1,0)の配列に変換
# 0の場合は0のまま(zeroを指定すれば変えられる)
# optionを指定することにより(1,0)を変えられる
# 破壊的メソッド
# もっと高速化できる?
def binarization(array, option=(1,0), zero=0):
    for i in range(len(array)):
        if array[i] > 0:
            array[i] = option[0]
        elif array[i] < 0:
            array[i] = option[1]
        else:
            array[i] = zero

# 1つの1次元配列を2つにわけたものを返す
# 最初の配列はlength長で,次の配列はそれ以外ののこり
# 非破壊的メソッド
def split_into_two(array, length):
    return [array[:length], array[length:]]

# 1つの1次元配列の先頭からlength個ずつ切ったものをnum個返す
# もし範囲を超える場合はraiseする
# 非破壊的メソッド
def split_into(array, length, num):
    if len(array) < length*num:
        raise Exception("Arguments exceeding the range of the array is set.")

    ret = []
    for i in range(num):
        ret.append(array[i*length:(i+1)*length])

    return ret

# 二つの1次元配列をつなげたものを返す
# 非破壊的メソッド
def joint_two(array1, array2):
    tmp = []
    tmp.extend(array1)
    tmp.extend(array2)
    return tmp

# n個の1次元配列をつなげたものを返す
# 非破壊的メソッド
def joint_all(arrays):
    tmp = []
    for a in arrays:
        tmp.extend(a)
    return tmp

# QIMを使って埋め込みを行う
# dは埋め込みを行う系列
# infoは-1と+1からな埋め込む情報
# embed_strは埋め込み強度
# backをTrueにすると後ろから埋め込む
# 破壊的メソッド
def embed_by_qim(d, info, embed_str, back=False):
    for i in range(len(info)):
        d_index = i
        if back:
            d_index = len(d)-len(info) + i

        tmp = math.floor( (float(d[d_index])+embed_str/2.) / embed_str )

        # infoが-1だったら0,+1だったら1になる変数
        w = 0
        if info[i] > 0:
            w = 1

        if tmp % 2 == w:
            d[d_index] = embed_str * tmp + (embed_str/2.0)
        else:
            d[d_index] = embed_str * tmp - (embed_str/2.0)

# embed_by_qimによって埋め込まれた情報を抽出する
# info_length分抽出を行う
# 返り値は-1と+1からなる抽出情報
# 非破壊的メソッド
def extract_by_qim(d, info_length, embed_str, back=False):
    out = []
    for i in range(info_length):
        d_index = i
        if back:
            d_index = len(d)-info_length + i

        tmp = math.floor( ((float(d[d_index])+embed_str/2.) / embed_str) + 0.5 )
        if tmp % 2 != 0:
            out.append(-1)
        else:
            out.append(+1)
    return out
