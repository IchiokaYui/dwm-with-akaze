# coding: UTF-8

# 以下のようにコマンドライン引数を指定して扱う
# python image_wm.py EMBED input (out)
# python image_wm.py EXTRACT input

import numpy as np
import sys
import time
import cv2
import itertools

import akaze
import dwm
import image_processing as ip
import matrix_sequence_linker as msl
import quality_metrics as qm
import rotate_robust as rr
import sequence_processing as sp
import ss_dwm as ss
import rep_code

import settings

# 共通の前処理
# 画像を画素値の配列として取得,タイマーのスタート,特徴点の取得
def common_preprocess(input_name, method):
    print("\n[common_preprocess]")
    print("input_name:", input_name)

    # 画像読み込み及び色空間を変換したものの取得
    width, height = ip.get_size(input_name)
    y, cr, cb = ip.get_ycbcr_array(input_name)

    # タイマはじまり
    time_start = time.time()

    # 画像特徴点の取得
    basis_points_num = settings.BASIS_POINTS_NUM if method == "embed" else settings.BASIS_POINTS_NUM_EX
    basis_points = akaze.robust_fps(input_name, basis_points_num)
    # akaze.imwrite_fps(input_name, basis_points)
    akaze.print_fps(basis_points)

    # 特徴点
    fps = basis_points
    # akaze.imwrite_fps(img_path, fps)
    # ranks = rr.fps_dist_ranks(fps)

    return width, height, y, cr, cb, time_start, fps

# 特徴点の組み合わせを返す
# 1つでも見つかったら処理を終了させるにはbreak_if_findをTrueに設定する
def common_comb_fps(fps, break_if_find):
    print("\n[common_comb_fps]")

    fps_comb = list(itertools.combinations(fps, 2))
    ret_comb = []
    for idx, fps_set in enumerate(fps_comb):
        np_pt1 = np.array(fps_set[0].pt)
        np_pt2 = np.array(fps_set[1].pt)
        dist = np.linalg.norm(np_pt2 - np_pt1)

        if settings.MIN_BLOCK_SIZE < dist:
            if break_if_find:
                return np_pt1, np_pt2
            else:
                ret_comb.append([np_pt1, np_pt2])

    if len(ret_comb) == 0:
        print("有効な特徴点の組み合わせが見つかりませんでした.")
        return False

    return ret_comb

# 画像と特徴点に対する共通の処理
# 特徴点2点から回転や拡大縮小を施し,必要なcrop_blocksなどを返す
def common_crop_blocks(y, base_p1, base_p2, method):
    print("\n[common_crop_blocks]")
    print("base points:", base_p1, base_p2)

    # 画像が(回転しても)切れないように枠を設置した画像を作成
    frame_size, field_img = rr.get_field_image(y)
    p1 = base_p1 + np.array([frame_size, frame_size])
    p2 = base_p2 + np.array([frame_size, frame_size])
    int_p1 = tuple(map(int, p1))
    int_p2 = tuple(map(int, p2))
    # cv2.line(field_img, int_p1, int_p2, (128, 128, 128), 2)
    # cv2.imwrite("field0.png", field_img)

    # 回転する角度,拡大するサイズ
    angle = rr.vec_angle(p1, p2)
    distance = np.linalg.norm(p1 - p2)
    resize_size = int(np.floor(np.ceil(distance/100)*100))
    resize_rate = resize_size/distance
    print("points:", p1, p2)
    print("angle:", angle)
    print("distance:", distance)
    print("resize_size:", resize_size)
    print("resize_rate:", resize_rate)

    # 角度に合わせて回転(ベクトルが水平になる)
    field_img = rr.rotate_img(field_img, -angle)
    p1_aft = rr.rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p1)
    p2_aft = rr.rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p2)
    # cv2.imwrite("field1.png", field_img)
    field_img_small = np.array(field_img)
    p1_aft_small = np.array(p1_aft)
    p2_aft_small = np.array(p2_aft)

    # 量子化サイズに合わせて拡大
    field_img = rr.resize_img(field_img, resize_rate)
    p1_aft *= resize_rate
    p2_aft *= resize_rate
    print("p1_aft:", p1_aft)
    print("p2_aft:", p2_aft)
    # cv2.imwrite("field1.5.png", field_img)

    # 展開, DCT
    basis_point = p1_aft
    local_block_size = int(round(p2_aft[0] - p1_aft[0]))
    print("local_block_size:", local_block_size) # 正の値になるはず
    crop_blocks, upper_left_size, upper_left_pt = msl.crop_blocks(field_img, local_block_size, basis_point)
    ip.dct2d_to_blocks(crop_blocks)

    # 返す
    if method == "extract":
        # 小さい方の量子化サイズを作成する
        print("----small----")

        resize_size_small = int(np.floor(np.floor(distance/100)*100))
        resize_rate_small = resize_size_small/distance
        print("resize_size_small:", resize_size_small)
        print("resize_rate_small:", resize_rate_small)

        field_img_small = rr.resize_img(field_img_small, resize_rate_small)
        p1_aft_small *= resize_rate_small
        p2_aft_small *= resize_rate_small
        print("p1_aft_small:", p1_aft_small)
        print("p2_aft_small:", p2_aft_small)

        basis_point = p1_aft_small
        local_block_size = int(round(p2_aft_small[0] - p1_aft_small[0]))
        print("local_block_size:", local_block_size) # 正の値になるはず
        crop_blocks_small, upper_left_size, upper_left_pt = msl.crop_blocks(field_img_small, local_block_size, basis_point)
        ip.dct2d_to_blocks(crop_blocks_small)

        # return None, None, None, None, { "large": crop_blocks, "small": crop_blocks_small }, None
        return None, None, None, None, { "large": crop_blocks, "small": [] }, None # こっちでもいいかもしれない

    elif method == "embed":
        return frame_size, angle, resize_rate, field_img, crop_blocks, upper_left_pt

def embed(input_name, output_name):
    width, height, y, cr, cb, time_start, fps = common_preprocess(input_name, "embed")

    # 特徴点の組み合わせを検証
    points = common_comb_fps(fps, True)
    if points == False:
        return False
    base_p1, base_p2 = points

    # 切り出し
    frame_size, angle, resize_rate, field_img, crop_blocks, upper_left_pt = common_crop_blocks(y, base_p1, base_p2, "embed")

    # 反復符号の符号化
    redundant_secret_info = rep_code.encode(settings.SECRET_INFO, settings.REDUNDANCY)

    # 量子化DCT-OFDM, 埋め込み
    for line in crop_blocks:
        for crop_block in line:

            # 周波数成分から系列を作成
            seq = msl.get_mid_freq_seq2(crop_block, settings.MID_FREQ_MIN, settings.MID_FREQ_MAX)
            seq = sp.resettable_shuffle(seq, settings.RANDOM_SEED)
            use_array, not_use_array = sp.split_into_two(seq, settings.USE_LEN)

            # 量子化DCT-OFDM方式を用いて埋め込み
            use_array = ip.dct1d(use_array * settings.PN_SERIES)
            sp.embed_by_qim(use_array, redundant_secret_info, settings.EMBED_STR)
            sp.embed_by_qim(use_array, settings.SYNC_INFO, settings.EMBED_STR_SYNC, back=True)
            use_array = ip.idct1d(use_array) * settings.PN_SERIES

            # 系列を周波数成分に戻す
            seq = sp.joint_two(use_array, not_use_array)
            seq = sp.reset_shuffle(seq, settings.RANDOM_SEED)
            msl.set_mid_freq_seq2(crop_block, seq, settings.MID_FREQ_MIN, settings.MID_FREQ_MAX)

    print("\n[embed postprocess]")

    # IDCT, 矩形をもどす
    ip.idct2d_to_blocks(crop_blocks)
    msl.paste_crop_blocks(field_img, upper_left_pt, crop_blocks)

    # 逆回転, 縮小, 切り抜き
    # cv2.imwrite("field2.png", field_img)
    field_img = rr.rotate_img(field_img, angle)
    # cv2.imwrite("field3.png", field_img)
    field_img = rr.resize_img(field_img, 1/resize_rate)
    # cv2.imwrite("field4.png", field_img)
    field_img = rr.trim_field_image(field_img, frame_size, [height, width])

    # タイマ終わり
    print("TIME: {0}".format(time.time() - time_start))

    # 保存
    ip.save_ycbcr_as_img(field_img, cr, cb, output_name)

    # 評価
    print("PSNR:", qm.psnr_by_file(input_name, output_name))
    # print("SSIM\t%f" % qm.ssim_by_file(input_name, output_name))

    return True

def extract(input_name, only_ber=False):
    width, height, y, cr, cb, time_start, fps = common_preprocess(input_name, "extract")

    # 最終的な抽出結果を算出するための結果を格納する
    extra_infos = []
    min_ber = 1.0

    # 特徴点の組み合わせ
    fps_comb = common_comb_fps(fps, False)
    if fps_comb == False:
        return False

    for [base_p1, base_p2] in fps_comb:

        tmp, tmp, tmp, tmp, two_crop_blocks, tmp = common_crop_blocks(y, base_p1, base_p2, "extract")

        for key, crop_blocks in two_crop_blocks.items():
            print("--------")
            print("key:", key)

            for line in crop_blocks:
                for crop_block in line:

                    # 周波数成分から系列を作成
                    seq = msl.get_mid_freq_seq2(crop_block, settings.MID_FREQ_MIN, settings.MID_FREQ_MAX)
                    seq = sp.resettable_shuffle(seq, settings.RANDOM_SEED)
                    use_array, not_use_array = sp.split_into_two(seq, settings.USE_LEN)

                    # 量子化DCT-OFDM方式を用いて抽出
                    use_array = ip.dct1d(use_array * settings.PN_SERIES)
                    info = sp.extract_by_qim(use_array, len(settings.SECRET_INFO)*settings.REDUNDANCY, settings.EMBED_STR)
                    sync_info = sp.extract_by_qim(use_array, len(settings.SYNC_INFO), settings.EMBED_STR_SYNC, back=True)
                    sync_info_ber = sp.ber(settings.SYNC_INFO, sync_info)
                    extra_infos.append([sync_info_ber, info])

                    info_decoded = rep_code.decode(info, settings.REDUNDANCY)
                    sp.binarization(info_decoded, option=(1,-1))
                    print("BER:", sp.ber(info_decoded, settings.SECRET_INFO), "SYNC_BER:", sync_info_ber)

                    if sync_info_ber < min_ber:
                        min_ber = sync_info_ber

    print("\n[extract postprocess]")

    # 抽出情報と同期信号のBERから最終的な抽出結果を算出
    last_extra_info = np.zeros(len(settings.SECRET_INFO)*settings.REDUNDANCY)
    for [ber, info] in extra_infos:
        last_extra_info += settings.WEIGHT(ber) * np.array(info)

    # 反復符号の復号
    last_extra_info = rep_code.decode(last_extra_info, settings.REDUNDANCY)
    sp.binarization(last_extra_info, option=(1,-1))
    print("min_ber:", min_ber)
    print("BER:", sp.ber(settings.SECRET_INFO, last_extra_info))

    # タイマ終わり
    print("TIME: {0}".format(time.time() - time_start))

    if only_ber:
        return sp.ber(settings.SECRET_INFO, last_extra_info)
    else:
        return min_ber, last_extra_info

if __name__ == "__main__":
    dwm.branch(embed, extract)
    print()
