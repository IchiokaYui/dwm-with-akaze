# coding: UTF-8 -W ignore

# 画像評価に関するモジュール
# 以下のプログラムをほとんど流用
# https://github.com/aizvorski/video-quality
# 読み込み時には以下のようにする
# import quality_metrics as qm

import numpy
import math
from scipy.ndimage import gaussian_filter
from numpy.lib.stride_tricks import as_strided as ast
from PIL import Image

import warnings
warnings.filterwarnings("ignore")

def _y_of_image(img_path):
    img = Image.open(img_path)
    y, cr, cb = img.convert("YCbCr").split()
    return numpy.asarray(y)

# PSNRの値を返す
# img1,img2は二次元配列(numpyの配列)
def psnr(img1, img2):
    mse = numpy.mean( (img1 - img2) ** 2 )
    if mse == 0:
        return float("inf")
    PIXEL_MAX = 255.0
    return 20 * math.log10(PIXEL_MAX / math.sqrt(mse))

# PSNRの値を返す
# img_path1,img_path2は画像のパス
def psnr_by_file(img_path1, img_path2):
    return psnr(_y_of_image(img_path1), _y_of_image(img_path2))

def _block_view(A, block=(3, 3)):
    shape = (A.shape[0]/ block[0], A.shape[1]/ block[1])+ block
    strides = (block[0]* A.strides[0], block[1]* A.strides[1])+ A.strides
    return ast(A, shape= shape, strides= strides)

# SSIMの値を返す
# img1,img2は二次元配列(numpyの配列)
def ssim(img1, img2, C1=0.01**2, C2=0.03**2):
    bimg1 = _block_view(img1, (4,4))
    bimg2 = _block_view(img2, (4,4))
    s1  = numpy.sum(bimg1, (-1, -2))
    s2  = numpy.sum(bimg2, (-1, -2))
    ss  = numpy.sum(bimg1*bimg1, (-1, -2)) + numpy.sum(bimg2*bimg2, (-1, -2))
    s12 = numpy.sum(bimg1*bimg2, (-1, -2))

    vari = ss - s1*s1 - s2*s2
    covar = s12 - s1*s2

    ssim_map =  (2*s1*s2 + C1) * (2*covar + C2) / ((s1*s1 + s2*s2 + C1) * (vari + C2))
    return numpy.mean(ssim_map)

# SSIMの値を返す
# img_path1,img_path2は画像のパス
def ssim_by_file(img_path1, img_path2):
    return ssim(_y_of_image(img_path1), _y_of_image(img_path2))