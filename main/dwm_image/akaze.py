# coding: UTF-8

# AKAZE画像特徴量に関する処理が記述されたmodule

import cv2

# AKAZE特徴量のロバストな点上位num個をimg_pathで指定された画像から取得する
# 非破壊的メソッド
def robust_fps(img_path, num):
    img = cv2.imread(img_path)
    detector = cv2.AKAZE_create()
    keypoints = detector.detect(img)
    keypoints.sort(key=lambda x:x.response, reverse=True)
    return keypoints[:num]

# input_nameの画像にfps(画像特徴点)を記述した画像を出力する
# out_nameを指定しなかった場合にはinput_name + ".fps.png"として画像を出力する
# 非破壊的メソッド
def imwrite_fps(input_name, fps, out_name=False):
    if out_name == False:
        out_name = input_name + ".fps.png"

    org_img = cv2.imread(input_name)
    cv2.drawKeypoints(org_img, fps, org_img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imwrite(out_name, org_img)

# fps(画像特徴点)を標準出力で出力する
# 非破壊的メソッド
def print_fps(fps):
    for i in range(len(fps)):
        fp = fps[i]
        print("index %d, postion (%lf, %lf), response %lf, size %lf" % (i, fp.pt[0], fp.pt[1], fp.response, fp.size))

def print_fp(fp):
    print("postion (%lf, %lf), response %lf, size %lf" % (fp.pt[0], fp.pt[1], fp.response, fp.size))

# # 非破壊的メソッド
# def draw_points(img_obj, points, color=[0,0,255], use_circle=False, circle_size=8, thickness=2):
#     tmp = img_obj.copy()
#     for point in points:
#         x = math.floor(point[0])
#         y = math.floor(point[1])
#         if not use_circle:
#             tmp[x, y] = color
#         else:
#             cv2.circle(tmp, (x, y), circle_size, color, thickness)
#     return tmp
