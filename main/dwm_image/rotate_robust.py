# coding: utf-8

import akaze
import numpy as np
import cv2
import sys
import scipy.misc

# それぞれのfpに対して距離の近い順に格納された配列がはいっている
# ranks = fps_dist_ranks(fps)
# ranks[0] # fps[0]の点から近い順にfpが格納された配列
def fps_dist_ranks(fps):
    ranks = []
    for point in fps:
        tmp_dists = []
        p1 = np.array(point.pt)
        for to_pt in fps:
            p2 = np.array(to_pt.pt)
            tmp_dists.append(np.linalg.norm(p1-p2))

        tmp_dists = np.array(tmp_dists)
        idx = np.argsort(tmp_dists)
        # print(tmp_dists[idx])
        tmp_fps = np.array(fps.copy())
        ranks.append(tmp_fps[idx])
    return ranks

# imgに枠を付けた画像を返す
# imgはcv2.imread()とかでえられる2次元配列
def get_field_image(img):
    height, width = img.shape[0:2]
    # frame_size = int((width**2+height**2)**0.5*0.5-min(width,height)*0.5 + 5) # 本来ならこれで大丈夫
    frame_size = int((width**2+height**2)**0.5*0.5-min(width,height)*0.5 + 5)*2 # 展開用に大きく
    # field_img = np.zeros(np.array(img.shape[0:2])+np.array([frame_size*2,frame_size*2,0)), dtype=np.uint8)
    field_img = np.zeros( ( np.array(img.shape[0:2])+np.array( [frame_size*2,frame_size*2] ) ), dtype=np.uint8 )
    field_img[frame_size:frame_size+height, frame_size:frame_size+width,] = img
    field_img = field_img.astype(np.float64)
    return frame_size, field_img

# imgから枠を削除した画像を返す
def trim_field_image(field_img, frame_size, org_size):
    height, width = org_size[0:2]
    return field_img[frame_size:frame_size+height, frame_size:frame_size+width,]

# p1,p2はnp.arrayとかの行列(x,y座標成分をもつ)
def vec_angle(p1, p2):
    u = p2-p1
    return np.angle(u[0]-u[1]*1j, deg=True)

# 画像をangle[deg]回転させた画像を返す
def rotate_img(img, angle):
    np.clip(img, 0, 255, out=img)
    img = scipy.misc.imrotate(img, angle)
    img = np.array(img)
    img = img.astype(np.float64)
    return img

# 画像をresize_rate倍に拡大(縮小)させた画像を返す
def resize_img(img, resize_rate):
    np.clip(img, 0, 255, out=img)
    img = scipy.misc.imresize(img, resize_rate, "lanczos")
    img = np.array(img)
    img = img.astype(np.float64)
    return img

# field_imageないの座標点ptをangle[deg]回転させた後の座標点を返す
def rotate_pt_with_frame(field_img_size, frame_size, angle, pt):
    center = tuple(np.array(field_img_size)/2)
    center = center[::-1]
    rot_mat = cv2.getRotationMatrix2D(center, angle, 1.0)
    return np.dot(rot_mat, np.r_[pt, [1]].T)

# 使い方サンプル
if __name__ == "__main__":
    # 画像の準備
    img_path = "Lenna.jpg"
    out_path = "out.png"
    img = cv2.imread(img_path)
    height, width = img.shape[0:2]

    # 特徴点の算出
    fps = akaze.robust_fps(img_path, 10)
    akaze.imwrite_fps(img_path, fps)
    ranks = fps_dist_ranks(fps)

    # 画像が切れないように枠を設置した画像を作成
    frame_size, field_img = get_field_image(img)
    p1 = np.array(fps[3].pt) + np.array([frame_size, frame_size])
    p2 = np.array(ranks[3][9].pt) + np.array([frame_size, frame_size])
    # int_p1 = tuple(map(int, p1))
    # int_p2 = tuple(map(int, p2))
    # cv2.line(field_img, int_p1, int_p2, (255, 255, 0), 2)
    cv2.imwrite("field0.png", field_img)

    # 回転する角度
    angle = vec_angle(p1, p2)
    print("angle:", angle)

    # 角度に合わせて回転(ベクトルが水平になる)
    field_img = rotate_img(field_img, -angle)
    p1_aft = rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p1)
    p2_aft = rotate_pt_with_frame(field_img.shape[0:2], frame_size, -angle, p2)
    # int_p1_aft = tuple(map(int, p1_aft))
    # int_p2_aft = tuple(map(int, p2_aft))
    # cv2.line(field_img, int_p1_aft, int_p2_aft, (0, 255, 255), 2)
    cv2.imwrite("field1.png", field_img)

    # 逆回転
    field_img = rotate_img(field_img, angle)
    cv2.imwrite("field2.png", field_img)

    # 切り抜き
    field_img = trim_field_image(field_img, frame_size, img.shape[0:2])
    cv2.imwrite("field3.png", field_img)
