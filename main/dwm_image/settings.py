import ss_dwm as ss
import sequence_processing as sp
import numpy as np

# 埋め込み基準点の数と推定埋め込み基準点の数
BASIS_POINTS_NUM = 5 # 埋め込み時使う特徴点の数
# BASIS_POINTS_NUM_EX = 5 # 抽出時使う特徴点の数
BASIS_POINTS_NUM_EX = 5 # 抽出時使う特徴点の数
MIN_BLOCK_SIZE = 300 # 特徴点と特徴点の距離の最小値(これ以上の幅をとらないといけない)

# 埋め込み用パラメータ
RANDOM_SEED = 123456
MID_FREQ_MIN = 150
MID_FREQ_MAX = 300

# 量子化DCT-OFDM方式用パラメータ
USE_LEN = 2**14
PN_SERIES = sp.pn_series(USE_LEN, seed=RANDOM_SEED)
SECRET_INFO = sp.pn_series(100, seed=RANDOM_SEED) # 増やすと画質が悪くなる
EMBED_STR = 100
SYNC_INFO = sp.pn_series(64, seed=RANDOM_SEED+1)
EMBED_STR_SYNC = 100
WEIGHT = lambda x: 0.01**(x*8)

# 反復符号用パラメータ
REDUNDANCY = 10

# shuffle用
SHUFFLE_KEY_LEN = 100
SHUFFLE_KEY_NUM = 2
SHUFFLE_KEYS = []
for idx in range(SHUFFLE_KEY_NUM):
    SHUFFLE_KEYS.append( sp.pn_series(SHUFFLE_KEY_LEN, seed=RANDOM_SEED+idx) )
