# 画像特徴量に基づく同期回復を用いた画像電子透かし
以下の説明内容は概要です。説明の詳しくはファイル先頭のコメントなどを参考に。

# 使い方
以下のコマンドで各処理を行う。

埋め込み処理(Lenna.png.embed.pngを書き出す)

```python image_wm.py EMBED Lenna.png```

抽出処理

```python image_wm.py EXTRACT Lenna.png.embed.png```

攻撃処理

```python attack.py Lenna.png.embed.png```


# メインのファイル
以下のファイルはメインの電子透かしの埋め込みや抽出をする際に使う。

|ファイル名|概要|
|-|-|
|image_wm.py|メインの画像電子透かしプログラム。埋め込みや抽出を行う。|
|settings.py|メインの画像電子透かしのパラメータなど|
|Lenna.png|言わずと知れたサンプル画像|
|README.md|このファイル(説明ファイル)|

以下のファイルはimage_wm.pyを少しいじっただけのプログラム。(おそらく使わない)
|image_wm_light.py|147~148行目が異なる。抽出時間が1/2の代わりにBERが少し高くなる。|
|image_wm_shuffle.py|埋め込み系列をランダムにしてから埋め込む(実験の結果から精度が変わらないことが分かっている)|

# モジュールなどに関するファイル
以下のファイルはほとんどの場合、個別に使用しない。

|ファイル名|概要|
|-|-|
|akaze.py|AKAZE画像特徴量に関するmodule|
|dwm.py|電子透かしとしてのコマンドラインアプリケーションの枠組みに関するmodule|
|image_processing.py|画像処理に関する処理が記述されたmodule(画素値についてやDCTなど)|
|matrix_sequence_linker.py|行列と数列に関する処理が記述されたmodule|
|quality_metrics.py|画像評価に関するmodule(PSNRやSSIMなど)|
|rotate_robust.py|特徴点を用いて回転に対する耐性を持たせるために用いるmodule|
|sequence_processing.py|数列処理に関する処理が記述されたmodule|
|ss_dwm.py|スペクトル拡散を利用した埋め込みのためのmodule|
|rep_code.py|反復符号を使うためのmodule|

詳しくはファイル先頭のコメントなどを参考に。

# テストや確認に関するファイル
以下のファイルはテストや確認の際に使う。

|ファイル名|概要|
|-|-|
|attack.py|攻撃を加えるプログラム|
|test_akaze_view.py|画像からAKAZE特徴量を算出したものを表示するだけ|

# その他
個別の実験などに用いた電子透かしを行うプログラム。個別に動作する(はず)。テストなどあまりしていないので、参考程度に。

|ファイル名|概要|
|-|-|
|simple_dct_ofdm.py|量子化DCT-OFDM方式を用いて埋め込み抽出を行うプログラム|
|fdm_dct_ofdm.py|simple_dct_ofdmを周波数多重化したもの(FDM=周波数分割多重化)|
|wm_rotate_robust.py|image_wm.pyの試作版(回転耐性があるだけ)|