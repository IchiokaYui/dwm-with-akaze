# imagemagickを使って画像変換を加えるモジュール
import subprocess
from PIL import Image

def cmd(cmd_str):
    subprocess.call(cmd_str, shell=True)

# 圧縮
# numは1から100(1が高圧縮)
def comp(input_path, num, output_path=None):
    if output_path == None:
        output_path = input_path + ".comp.%s.png" % num
    cmd('convert -quality %s %s %s' % (num, input_path, output_path))
    return output_path

# 拡大縮小
# numは拡大率[%]
def resize(input_path, num, output_path=None):
    if output_path == None:
        output_path = input_path + ".resize.%s.png" % num
    cmd('convert -resize %s%% %s %s' % (num, input_path, output_path))
    return output_path

# 回転(縁有り)
def rotate(input_path, num, output_path=None):
    if output_path == None:
        output_path = input_path + ".rotate.%s.png" % num
    cmd('convert -rotate %s %s %s' % (num, input_path, output_path))
    return output_path

# ぼかし
# numはradiusとsigma numxnumのフィルタ
def blur(input_path, num, output_path=None):
    if output_path == None:
        output_path = input_path + ".blur.%s.png" % num
    cmd('convert -blur %sx%s %s %s' % (num, num, input_path, output_path))
    return output_path

# 切り抜き
# numは0から100
# 100だとほぼ切り抜かずに,0に近づくにつれ小さくなる
def crop(input_path, num, output_path=None):
    img = Image.open(input_path)
    width, height = img.size
    small_width = int(width * num/100)
    small_height = int(height * num/100)
    start_x = int( (width - small_width) / 2)
    start_y = int( (height - small_height) / 2)

    if output_path == None:
        output_path = input_path + ".crop.%s.png" % num
    cmd('convert -crop %sx%s+%s+%s %s %s' % (small_width, small_height, start_x, start_y, input_path, output_path))
    return output_path

# 画像の中央に四角を描く
# numは0から100
# 100だと黒塗りに,0に近づくにつれ小さくなる
def rect(input_path, num, output_path=None):
    img = Image.open(input_path)
    width, height = img.size
    small_width = int(width * num/100)
    small_height = int(height * num/100)
    start_x = int( (width - small_width) / 2)
    start_y = int( (height - small_height) / 2)

    if output_path == None:
        output_path = input_path + ".rect.%s.png" % num
    cmd("convert -draw 'rectangle %s,%s %s,%s' %s %s" % (start_x, start_y, small_width+start_x, small_height+start_y, input_path, output_path))
    return output_path

# 中間値
def median(input_path, num, output_path=None):
    if output_path == None:
        output_path = input_path + ".median.%s.png" % num
    cmd('convert -median %s %s %s' % (num, input_path, output_path))
    return output_path

# 全部の攻撃を与える
def various(input_path):
    return [
        comp(input_path, 10),
        comp(input_path, 30),
        # comp(input_path, 50),
        resize(input_path, 80),
        resize(input_path, 90),
        # resize(input_path, 95),
        # resize(input_path, 105),
        resize(input_path, 110),
        resize(input_path, 120),
        # rotate(input_path, 1),
        rotate(input_path, 3),
        rotate(input_path, 5),
        blur(input_path, 1),
        # blur(input_path, 2),
        blur(input_path, 3),
        # crop(input_path, 90),
        crop(input_path, 100),
        crop(input_path, 85),
        crop(input_path, 70),
        rect(input_path, 10),
        rect(input_path, 30),
    ]

if __name__ == "__main__":
    in_file = "./Lenna.png"
    files = various(in_file)
    print(files)
