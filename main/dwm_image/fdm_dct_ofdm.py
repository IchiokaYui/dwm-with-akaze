# coding: UTF-8

# 量子化DCT-OFDM方式を用いて埋め込み抽出を行うプログラム
# 画像は正方形が前提
# simple_dct_ofdmを周波数多重化しただけ(意味あるのか？)
# BASIS_POINTS_NUM=1としたらsimple_dct_ofdmと一緒
# 以下のようにコマンドライン引数を指定して扱う
# python fdm_dct_ofdm.py EMBED input (out)
# python fdm_dct_ofdm.py EXTRACT input

import image_processing as ip
import matrix_sequence_linker as msl
import sequence_processing as sp
import dwm
import numpy as np

BASIS_POINTS_NUM = 5
BASIS_FREQ_NUM = 100
SEED = 16606
FREQ_MIN = 0.25
FREQ_MAX = 0.5
EMBED_STR = 50
SECRET_INFO = [-1]*50 + [+1]*50
PN_SERIES = sp.pn_series(BASIS_FREQ_NUM, seed=SEED)

def embed(input_name, output_name):
    # 画像読み込み,色空間を変換したもの,周波数成分,画像の大きさの取得
    width, height = ip.get_size(input_name)
    if width != height: raise
    size = width
    y, cr, cb = ip.get_ycbcr_array(input_name)
    y = ip.dct2d(y)

    # 埋め込む周波数領域のリスト(BASIS_POINTS_NUM種類)の取得
    address_matrix = msl.address_2dmatrix(size)
    address_list = msl.get_mid_freq_seq(address_matrix, FREQ_MIN, FREQ_MAX)
    random_address_list = sp.resettable_shuffle(address_list, SEED)
    basis_freqs = sp.split_into(random_address_list, BASIS_FREQ_NUM, BASIS_POINTS_NUM)

    # それぞれの埋め込む周波数領域に関して
    for basis_freq in basis_freqs:
        # 周波数成分の配列を取得
        seq = msl.get_seq_by_address(y, basis_freq)

        # 量子化DCT-OFDM方式を用いて埋め込み
        seq = ip.dct1d(seq * PN_SERIES)
        sp.embed_by_qim(seq, SECRET_INFO, EMBED_STR)
        seq = ip.idct1d(seq) * PN_SERIES

        # 周波数成分の配列を戻す
        msl.set_seq_by_address(y, basis_freq, seq)

    # 画素値を0から255内に設定して保存
    y = ip.idct2d(y)
    msl.set_in_range(y, 0, 255)
    ip.save_ycbcr_as_img(y, cr, cb, output_name)

def extract(input_name):
    # 画像読み込み,色空間を変換したもの,周波数成分,画像の大きさの取得
    width, height = ip.get_size(input_name)
    if width != height: raise
    size = width
    y, cr, cb = ip.get_ycbcr_array(input_name)
    y = ip.dct2d(y)

    # 埋め込む周波数領域のリスト(BASIS_POINTS_NUM種類)の取得
    address_matrix = msl.address_2dmatrix(size)
    address_list = msl.get_mid_freq_seq(address_matrix, FREQ_MIN, FREQ_MAX)
    random_address_list = sp.resettable_shuffle(address_list, SEED)
    basis_freqs = sp.split_into(random_address_list, BASIS_FREQ_NUM, BASIS_POINTS_NUM)

    # それぞれの埋め込む周波数領域に関して
    for basis_freq in basis_freqs:
        # 周波数成分の配列を取得
        seq = msl.get_seq_by_address(y, basis_freq)
        seq = ip.dct1d(seq * PN_SERIES)

        # 以上までembed()と同じ
        # 以下抽出処理
        info = sp.extract_by_qim(seq, len(SECRET_INFO), EMBED_STR)
        print("BER: %lf" % (sp.hamming_dist(SECRET_INFO, info) / float(len(SECRET_INFO))))

if __name__ == "__main__":
    dwm.branch(embed, extract)
