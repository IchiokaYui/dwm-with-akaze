# coding: UTF-8

# 電子透かしに関する処理が記述されたmodule
# 主に計算などではなくフレームワークのようなものを提供する

import sys

# argvはコマンドライン引数のリスト(sys.argv)
# 以下のような実行をした場合のaction(EMBED,EXTRACT,STATUS)とinputとoutの文字列を返す
# python *.py EMBED input (out)
# python *.py EXTRACT input
# python *.py STATUS input
# それ以外だった場合はExceptionを返す
# outが省略された場合はinputの文字列のあとにextをつけたものをoutの文字列とする
# show_flagがTrueならば実際の値をprintする
# 非破壊的メソッド
def get_action(argv=sys.argv, ext=".embed.png", show_flag=False):
    action = ""
    input_name = ""
    output_name = ""

    try:
        action = argv[1]
        input_name = argv[2]
    except:
        raise Exception("Command-line arguments are not set correctly.")

    output_name = ""
    if action == "EMBED":
        if len(argv) > 3:
            output_name = argv[3]
        else:
            output_name = input_name + ext
    elif action == "EXTRACT":
        pass
    elif action == "STATUS":
        pass
    else:
        raise Exception("Command-line arguments are not set correctly.")

    if show_flag:
        print("%s, %s, %s" % (action, input_name, output_name))

    return [action, input_name, output_name]

# get_actionを利用して,分岐して処理を行う
# 引数に関数をとる
# この関数を使う場合にはget_actionを使う必要はほぼないはず
# embed_funcはembed(input_name, output_name)のように引数をとる関数
# extract_funcはextract(input_name)のように引数をとる関数
# 非破壊的メソッド
def branch(embed_func, extract_func):
    action, input_name, output_name = get_action()
    if action == "EMBED":
        embed_func(input_name, output_name)
    elif action == "EXTRACT":
        extract_func(input_name)
