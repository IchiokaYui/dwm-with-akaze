# 反復符号を使うためのmodule
# https://ja.wikipedia.org/wiki/%E5%8F%8D%E5%BE%A9%E7%AC%A6%E5%8F%B7

import numpy as np

# 符号化
# 非破壊的
def encode(info, redundancy):
    return np.tile(np.matrix(info), (redundancy, 1)).T.ravel().tolist()[0]

# 復号化
# 非破壊的
def decode(info, redundancy):
    tmp = np.array(info).reshape( (int(len(info) / redundancy), redundancy) )
    return np.average(tmp, axis=1).tolist()

if __name__ == "__main__":
    a = [1,2,3,10]
    red = 5

    print(a)
    b = encode(a, red)
    print(b)
    c = decode(b, red)
    print(c)
