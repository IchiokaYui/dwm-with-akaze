# coding: UTF-8

import akaze
import sys

filename = sys.argv[1]
fps = akaze.robust_fps(filename, 50)
akaze.imwrite_fps(filename, fps)
akaze.print_fps(fps)
