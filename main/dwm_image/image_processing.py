# coding: UTF-8

# 画像処理に関する処理が記述されたmodule

from PIL import Image
import numpy as np
from scipy.fftpack import dct, idct
import math

# 画像のサイズを返す
# nameは画像ファイル名
# 非破壊的メソッド
def get_size(name):
    pil_img = Image.open(name)
    return [pil_img.width, pil_img.height]

# nameは画像ファイル名
# 画像のYとCrとCbの行列(numpyの行列)を返す
# 非破壊的メソッド
def get_ycbcr_array(name):
    pil_img = Image.open(name)
    pil_y, pil_cr, pil_cb = pil_img.convert("YCbCr").split()
    y = np.asarray(pil_y)
    cr = np.asarray(pil_cr)
    cb = np.asarray(pil_cb)
    y.flags.writeable = True
    cr.flags.writeable = True
    cb.flags.writeable = True
    return [y, cr, cb]

def get_rgb_array(name):
    pil_img = Image.open(name)
    rgb = np.array(pil_img)
    r = rgb[:, :, 0]
    g = rgb[:, :, 1]
    b = rgb[:, :, 2]
    r = r.astype(np.float64)
    g = g.astype(np.float64)
    b = b.astype(np.float64)
    return [r, g, b]

# # 例: save_element_as_img(y, name="y.png")
def save_element_as_img(ycbcr_element_array, name):
    pil_img = Image.fromarray(np.uint8(ycbcr_element_array))
    pil_img.save(name)

# *_arrayはnumpyの行列
# nameは出力画像ファイル名
# 指定したYとCrとCbの行列から画像を書き出す
# 非破壊的メソッド
def save_ycbcr_as_img(y_array, cr_array, cb_array, name):
    np.clip(y_array, 0, 255, out=y_array)
    np.clip(cr_array, 0, 255, out=cr_array)
    np.clip(cb_array, 0, 255, out=cb_array)
    pil_y = Image.fromarray(np.uint8(y_array))
    pil_cr = Image.fromarray(np.uint8(cr_array))
    pil_cb = Image.fromarray(np.uint8(cb_array))
    pil_img = Image.merge("YCbCr", (pil_y, pil_cr, pil_cb)).convert("RGB")
    pil_img.save(name)

def save_rgb_as_img(r_array, g_array, b_array, name):
    width = len(r_array[0])
    height = len(r_array)
    np.clip(r_array, 0, 255, out=r_array)
    np.clip(g_array, 0, 255, out=g_array)
    np.clip(b_array, 0, 255, out=b_array)
    # r_array = r_array.astype(np.uint8)
    # g_array = g_array.astype(np.uint8)
    # b_array = b_array.astype(np.uint8)
    tmp = np.zeros((height, width, 3))
    tmp[:, :, 0] = r_array
    tmp[:, :, 1] = g_array
    tmp[:, :, 2] = b_array
    tmp = np.uint8(tmp)
    pil_img = Image.fromarray(tmp)
    pil_img = pil_img.convert('RGB')
    pil_img.save(name)

# 2次元DCTを行った結果(2次元行列)を返す
# 出力はnumpyの行列
# xは正方行列(numpyの行列)
# https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.fftpack.dct.html
# 非破壊的メソッド
def dct2d(x):
    return dct(dct(x.T).T) / (2*len(x))

# 長方形対応
def dct2d2(x):
    return dct(dct(x.T).T) / ( math.sqrt(2*len(x) * 2*len(x[0])) )

# 2次元IDCTを行った結果(2次元行列)を返す
# 出力はnumpyの行列
# xは正方行列(numpyの行列)
# https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.fftpack.idct.html
# 非破壊的メソッド
def idct2d(x):
    return idct(idct(x.T).T) / (2*len(x))

# 長方形対応
def idct2d2(x):
    return idct(idct(x.T).T) / ( math.sqrt(2*len(x) * 2*len(x[0])) )

# 1次元DCTを行った結果を返す
# 出力はnumpyの行列
# 非破壊的メソッド
def dct1d(x):
    return dct(x) / math.sqrt(2*len(x))

# 1次元IDCTを行った結果を返す
# 出力はnumpyの行列
# 非破壊的メソッド
def idct1d(x):
    return idct(x) / math.sqrt(2*len(x))

# blocks(2次元行列)の中の要素の2次元行列のすべてにdct2dを適用する
# 破壊的メソッド
def dct2d_to_blocks(blocks):
    for y in range(len(blocks)):
        for x in range(len(blocks[0])):
            blocks[y][x] = dct2d(blocks[y][x])

# blocks(2次元行列)の中の要素の2次元行列のすべてにidct2dを適用する
# 破壊的メソッド
def idct2d_to_blocks(blocks):
    for y in range(len(blocks)):
        for x in range(len(blocks[0])):
            blocks[y][x] = idct2d(blocks[y][x])
