# スペクトル拡散を利用した埋め込みのためのライブラリ
# 埋め込む情報量がn[bit]だった場合、
# 乱数の数列を2^n個つくる
# 埋め込み時に任意の場所に埋め込む値に対応した乱数の数列を足しこむ
# 抽出時にはすべてと相関をとり、判断する
# かなり原始的なやつ

import numpy as np

# +1と-1から成るlength長の乱数系列(numpyの1次元行列)を返す
# seedを指定した場合,seedで初期化する
# 非破壊的メソッド
def pn_series(length, seed=None):
    if seed != None:
        np.random.seed(seed=seed)
    bin_val = [-1, 1]
    return np.random.choice(bin_val, length)

# 長さ配列array_lenの配列を返す
# それぞれの要素は-1と1でつくった乱数の配列(長さn)
def pn_array(array_len, n, seed=None):
    if seed != None:
        np.random.seed(seed=seed)
    bin_val = [-1, 1]
    return np.random.choice(bin_val, (array_len,n))

# 例:
# bit_array = [-1 -1  1  1 -1 -1  1 -1]
# a = bin_array_to_num(bit_array)
# print(a) # => 50
def bin_array_to_num(bit_array):
    d = np.clip(bit_array, 0, 1)
    d = map(str, d)
    s = "".join(d)
    return int(s, 2)

# 以下メモ用後で消す

# # 設定
# N_OF_SEQ_LEN = 25000
# RANDOM_SEED = 123456
# BIT_NUM = 2**8
# A_OF_STR = 0.4
# EMBED_DATA = pn_series(16, 111111)
# MID_FREQ_MIN = 50
# MID_FREQ_MAX = 150
#
# def embed(input_name, output_name):
#     # 画像の読み込みとDCT
#     print("input_name", input_name, "output_name", output_name)
#     # print("画像の読み込みとDCT")
#     image_r, image_g, image_b = ip.get_rgb_array(input_name)
#     image_b = ip.dct2d2(image_b)
#
#     # エンコード
#     # print("エンコード")
#     X_OF_ENC = pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED)
#     Y_OF_ENC = pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED+1)
#
#     # 使う乱数列
#     # print("使う乱数列")
#     X_INDEX = bin_array_to_num(EMBED_DATA[:8])
#     Y_INDEX = bin_array_to_num(EMBED_DATA[8:])
#     # print("EMBED_DATA", EMBED_DATA)
#     # print("X_INDEX", EMBED_DATA[:8], X_INDEX)
#     # print("Y_INDEX", EMBED_DATA[8:], Y_INDEX)
#
#     # 足しこみ
#     # print("足しこみ")
#     mid_freq = msl.get_mid_freq_seq2(image_b, MID_FREQ_MIN, MID_FREQ_MAX)
#     # print("len(mid_freq)", len(mid_freq))
#     # print("N_OF_SEQ_LEN", N_OF_SEQ_LEN)
#     if len(mid_freq) > N_OF_SEQ_LEN: # 警告
#         print("N_OF_SEQ_LENがlen(mid_freq)より小さいです.")
#     # 埋め込む乱数の長さを合わせたりする
#     tmp_x = X_OF_ENC[X_INDEX][:len(mid_freq)]
#     tmp_y = Y_OF_ENC[Y_INDEX][:len(mid_freq)]
#     mid_freq = mid_freq + A_OF_STR * np.absolute(mid_freq) * ((tmp_x + tmp_y)/2)
#     msl.set_mid_freq_seq2(image_b, mid_freq, MID_FREQ_MIN, MID_FREQ_MAX)
#
#     # IDCTと保存
#     # print("IDCTと保存")
#     image_b = ip.idct2d2(image_b)
#     # ip.save_element_as_img(image_b, "b.png")
#     ip.save_rgb_as_img(image_r, image_g, image_b, output_name)
#
# def extract(input_name):
#     # 画像の読み込みとDCT
#     print("input_name", input_name)
#     # print("画像の読み込みとDCT")
#     image_r, image_g, image_b = ip.get_rgb_array(input_name)
#     image_b = ip.dct2d2(image_b)
#
#     # エンコード
#     # print("エンコード")
#     X_OF_ENC = pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED)
#     Y_OF_ENC = pn_array(BIT_NUM, N_OF_SEQ_LEN, RANDOM_SEED+1)
#
#     # 相関
#     mid_freq = msl.get_mid_freq_seq2(image_b, MID_FREQ_MIN, MID_FREQ_MAX)
#     X_INDEX = bin_array_to_num(EMBED_DATA[:8])
#     Y_INDEX = bin_array_to_num(EMBED_DATA[8:])
#     corre_x = []
#     corre_y = []
#
#     # たぶんもっと速くかける
#     for idx in range(BIT_NUM):
#         tmp_x = X_OF_ENC[idx][:len(mid_freq)]
#         tmp_y = Y_OF_ENC[idx][:len(mid_freq)]
#         corre_x.append( np.dot(tmp_x, mid_freq) )
#         corre_y.append( np.dot(tmp_y, mid_freq) )
#
#     corre_x = np.array(corre_x) / len(mid_freq)
#     corre_y = np.array(corre_y) / len(mid_freq)
#     # np.savetxt("corre_x.csv", corre_x, delimiter=",")
#     # np.savetxt("corre_y.csv", corre_y, delimiter=",")
#
#     # 閾値を超える番地を探して、抽出
#     # print("閾値を超える番地を探して、抽出")
#     # threshold = 1/(20*len(mid_freq)) * np.absolute(mid_freq).sum()
#     # x_index = np.where(corre_x > threshold)
#     # y_index = np.where(corre_y > threshold)
#     # print("x_index", x_index)
#     # print("y_index", y_index)
#
#     # 相関値の合計(適応フレーム選択用)
#     X_INDEX = bin_array_to_num(EMBED_DATA[:8])
#     Y_INDEX = bin_array_to_num(EMBED_DATA[8:])
#     # print("X_INDEX", EMBED_DATA[:8], X_INDEX)
#     # print("Y_INDEX", EMBED_DATA[8:], Y_INDEX)
#     ext_res_x = np.dot( mid_freq, X_OF_ENC[X_INDEX][:len(mid_freq)] ).sum() / len(mid_freq)
#     ext_res_y = np.dot( mid_freq, Y_OF_ENC[Y_INDEX][:len(mid_freq)] ).sum() / len(mid_freq)
#     # print("ext_res_x" , ext_res_x)
#     # print("ext_res_y" , ext_res_y)
#     # print("ext_res_x + ext_res_y" , ext_res_x + ext_res_y)
#     # print("extractor_response", ext_res_x + ext_res_y)
#     return ext_res_x + ext_res_y
