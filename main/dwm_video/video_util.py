import subprocess
import os

# https://orebibou.com/2016/12/python%E3%81%AEsubprocess%E3%81%A7%E3%82%B3%E3%83%9E%E3%83%B3%E3%83%89%E3%81%AE%E5%AE%9F%E8%A1%8C%E7%B5%90%E6%9E%9C%E3%82%92%E5%87%BA%E5%8A%9B%E3%81%95%E3%81%9B%E3%81%AA%E3%81%84/
devnull = open('/dev/null', 'w')

# video_pathで指定した動画をコマ画像に分割する
# frames_pathで指定したフォルダ内にコマ画像を連番で入れる
def divide_video(video_path, frames_path):
    subprocess.run( ["ffmpeg", "-i", video_path, "-vcodec", "png", frames_path + "/%06d.png"], stdout=devnull, stderr=devnull )

# divide_videoの逆
# コマから動画をつくる
# フレームレートはプログラム内で固定しているので、場合によっては書き換え必須
def join_frames(frames_path, video_path, ext="png"):
    # 標準品質(かなり圧縮がかかる)
    # subprocess.run( ["ffmpeg", "-framerate", "25", "-i", frames_path + "/%06d." + ext, "-vcodec", "libx264", "-pix_fmt", "yuv420p", "-r", "25", video_path] )
    # 最高品質
    subprocess.run( ["ffmpeg", "-framerate", "25", "-i", frames_path + "/%06d." + ext, "-crf", "0", "-vcodec", "libx264", "-pix_fmt", "yuv420p", "-r", "25", video_path] )

# frames_pathのコマ一枚一枚に対して関数を実行できる
# funcの例
# def test(idx, path):
#     print(idx, path)
# 上記の例だと、frames_pathの中身の画像のパスを順番に表示する
def enumerate_frames(frames_path, func, ext="png"):
    files = os.listdir(frames_path)
    # print("frames num", len(files))
    for count in range(len(files)):
        frame_path = frames_path + "/%06d." % (count + 1) + ext
        func(count, frame_path)
