import sys
import json
import os
import numpy as np
sys.path.append("../dwm_image")
import settings
import sequence_processing as sp

JSON_TARGET_DIR = sys.argv[1]

if __name__ == "__main__":
    # ディレクトリ内のjsonファイルをリストに
    files = os.listdir(JSON_TARGET_DIR)
    target_files = []

    for file_path in files:
        root, ext = os.path.splitext(file_path)
        if ext != ".json":
            continue
        target_files.append(file_path)

    target_files.sort()

    # 全部のファイルを予め読み込み
    file_data = []

    for target_file in target_files:
        with open(JSON_TARGET_DIR + "/" + target_file, 'r') as f:
            file_data.append(json.load(f))

    # # パラメータ総当たり
    # # average.txt
    # # a_list = np.arange(0.01, 0.5+0.01, 0.01)
    # # b_list = np.arange(1, 100+1, 1)
    # a_list = np.arange(0.0001, 0.01+0.0001, 0.0001)
    # b_list = np.arange(0.1, 10+0.1, 0.1)
    # #
    # print("a_list:", a_list)
    # print("b_list:", b_list)
    #
    # for a in a_list:
    #     tmp = "[{}]\t".format(a)
    #     for b in b_list:
    #         weight = lambda x: a**(x*b)
    #
    #         bers = []
    #         for extra_infos in file_data:
    #             last_extra_info = np.zeros(len(settings.SECRET_INFO))
    #             for [ber, info] in extra_infos:
    #                 last_extra_info += weight(ber) * np.array(info)
    #             sp.binarization(last_extra_info, option=(1,-1))
    #             ber = sp.ber(settings.SECRET_INFO, last_extra_info)
    #             bers.append(ber)
    #
    #         ave = np.average(bers)
    #         tmp += str(ave) + "\t"
    #     print(tmp)

    # それぞれのBERを出力
    weight = lambda x: 0.01**(x*8)

    for idx, target_file in enumerate(target_files):
        extra_infos = file_data[idx]

        last_extra_info = np.zeros(len(settings.SECRET_INFO))
        for [ber, info] in extra_infos:
            last_extra_info += weight(ber) * np.array(info)
        sp.binarization(last_extra_info, option=(1,-1))
        ber = sp.ber(settings.SECRET_INFO, last_extra_info)

        print(target_file, ber)
