import sys
import video_util
import subprocess
import os
import shutil

TARGET_FILE = sys.argv[1]

def cmd(cmd_str):
    subprocess.call(cmd_str, shell=True)

def comp_video(frame_path, num):
    def tmp_func(count, path):
        cmd_str = 'convert -quality %s %s %s' % ( num, path, path.replace(".png", ".jpg") )
        cmd(cmd_str)
    video_util.enumerate_frames(frame_path, tmp_func, ext="png")
    video_util.join_frames(frame_path, frame_path + ".mp4", ext="jpg")

def resize_video(frame_path, num):
    def tmp_func(count, path):
        cmd_str = 'convert -resize %s%% %s %s' % ( num, path, path )
        cmd(cmd_str)
    video_util.enumerate_frames(frame_path, tmp_func, ext="png")
    video_util.join_frames(frame_path, frame_path + ".mp4", ext="png")

def rotate_video(frame_path, num):
    def tmp_func(count, path):
        cmd_str = 'convert -rotate %s %s %s' % ( num, path, path )
        cmd(cmd_str)
    video_util.enumerate_frames(frame_path, tmp_func, ext="png")
    video_util.join_frames(frame_path, frame_path + ".mp4", ext="png")

if __name__ == "__main__":

    # フォルダの用意
    tmp_dir_name = TARGET_FILE + "." + "attack"
    if os.path.isdir(tmp_dir_name):
        shutil.rmtree(tmp_dir_name)
    os.mkdir(tmp_dir_name)

    # コマ割り
    video_util.divide_video(TARGET_FILE, tmp_dir_name)

    # 個別に作業フォルダの作成
    shutil.copytree(tmp_dir_name, tmp_dir_name+".comp.10")
    shutil.copytree(tmp_dir_name, tmp_dir_name+".comp.30")
    shutil.copytree(tmp_dir_name, tmp_dir_name+".resize.90")
    shutil.copytree(tmp_dir_name, tmp_dir_name+".resize.110")
    shutil.copytree(tmp_dir_name, tmp_dir_name+".rotate.3")

    # 個別に攻撃して動画作成
    comp_video(tmp_dir_name+".comp.10", 10)
    comp_video(tmp_dir_name+".comp.30", 30)
    resize_video(tmp_dir_name+".resize.90", 90)
    resize_video(tmp_dir_name+".resize.110", 110)
    rotate_video(tmp_dir_name+".rotate.3", 3)
