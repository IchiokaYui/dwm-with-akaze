# 画像特徴量に基づく同期回復を用いた動画電子透かし
以下の説明内容は概要です。説明の詳しくはファイル先頭のコメントなどを参考に。

# 使い方
以下のコマンドで各処理を行う。

埋め込み処理

```python video_wm.py EMBED video.mp4```

抽出処理

```python video_wm.py EXTRACT video.mp4.embed.mp4```

攻撃処理

```python video_attack.py video.mp4.embed.mp4```


# ファイルの説明
以下のファイルはメインの電子透かしの埋め込みや抽出をする際に使う。
多くのプログラムがdwm_imageのプログラムを参照して使っているので注意が必要。

|ファイル名|概要|
|-|-|
|video_wm.py|メインの画像電子透かしプログラム。埋め込みや抽出を行う。|
|video_attack.py|動画への攻撃プログラム(様々な攻撃を加えた動画を書き出す)|
|README.md|このファイル(説明ファイル)|
|json_extract.py|抽出情報(.jsonファイル)から抽出処理行う。|
|video_util.py|動画とフレームの変換など処理を集めたのmodule|

以下のファイルはvideo_wm.pyを少しいじっただけのプログラム。(おそらく使わない)
|video_wm_shuffle.py|埋め込み系列をランダムにしてから埋め込む(実験の結果から精度が変わらないことが分かっている)|

