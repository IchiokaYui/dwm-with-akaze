# coding: UTF-8

# 以下のようにコマンドライン引数を指定して扱う
# python video_wm.py EMBED input (out)
# python video_wm.py EXTRACT input

import sys
import video_util
import time
import shutil
import os
import io
import numpy as np
sys.path.append("../dwm_image")
import importlib
import dwm
import json
# image_wm = importlib.import_module('image_wm')
image_wm = importlib.import_module('image_wm_shuffle')
import sequence_processing as sp
import settings

# 共通の前処理
def common_preprocess(input_name, method):
    print("\n[common_preprocess]")
    print("input_name:", input_name)

    start_time = time.time()

    # フォルダの用意
    tmp_dir_name = input_name + "." + method
    if os.path.isdir(tmp_dir_name):
        shutil.rmtree(tmp_dir_name)
    os.mkdir(tmp_dir_name)

    # 動画分割
    video_util.divide_video(input_name, tmp_dir_name)

    return start_time, tmp_dir_name

def embed(input_name, output_name):
    start_time, dir_name = common_preprocess(input_name, "embed")

    # 埋め込み
    results = []
    def image_embed(count, frame_path):
        print(count, frame_path)
        result = image_wm.embed(frame_path, frame_path)
        results.append(result)

    video_util.enumerate_frames(dir_name, image_embed)

    # フレーム結合
    if os.path.isfile(output_name):
        os.remove(output_name)
    video_util.join_frames(dir_name, output_name)

    # タイマ終わり
    print("TIME: {0}".format(time.time() - start_time))

def extract(input_name):
    start_time, dir_name = common_preprocess(input_name, "extract")

    # 抽出
    extra_infos = []
    def image_extract(count, frame_path):
        print(count, frame_path)

        sys.stdout = io.StringIO()
        result = image_wm.extract(frame_path)
        sys.stdout = sys.__stdout__

        if result != False:
            # extra_infos.append(result)
            extra_infos.append([result[0], result[1].tolist()])

    video_util.enumerate_frames(dir_name, image_extract)

    # データを書き出し
    f = open(input_name+".json", "w")
    json.dump(extra_infos, f, ensure_ascii=False, indent=4, sort_keys=True, separators=(',', ': '))
    f.close()

    # 最終的な抽出結果を算出
    last_extra_info = np.zeros(len(settings.SECRET_INFO))
    for [ber, info] in extra_infos:
        last_extra_info += settings.WEIGHT(ber) * np.array(info)
    sp.binarization(last_extra_info, option=(1,-1))
    print("BER:", sp.ber(settings.SECRET_INFO, last_extra_info))

    # タイマ終わり
    print("TIME: {0}".format(time.time() - start_time))

if __name__ == "__main__":
    action, input_name, output_name = dwm.get_action(ext=".embed.mp4")
    if action == "EMBED":
        embed(input_name, output_name)
    elif action == "EXTRACT":
        extract(input_name)
